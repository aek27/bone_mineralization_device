%aek27
%Plots for BME 790 Intended Signal Shapes
%Oh wow it's been a long time since I coded anything
x = linspace(0,0.000010,100000);%units is seconds %so this is 0.1 milliseconds
%Arduino clock speed is normally 16 MHz so even if we lower 
%assume our microcontroller can control down to the millisecond
sine = zeros(1, length(x));
%noisy = zeros(1, length(X));
for l = 1:length(x)-1
  
        sine(l) =  3*sin(5000000*x(l));%we don't actually know magnitude of voltage yet- assume 0.2 is "small" for now
        
end
noisy = awgn(sine,20);
%plot( x, abs(noisy), 'b-')
axis([0 1e-5 -4 4])
fwr = abs(noisy);
%now let's do a moving average (like a low pass filter would do
ma = zeros(1, length(fwr));
for k = 1:10000
    ma(k) = fwr(k);
end
for k = 10001:length(fwr)-10000
    ma(k) = mean(fwr([k-10000:k+10000]));
end
figure(2)
plot(x, ma)
axis([0.2e-5 0.8e-5 0 3])
xlabel('Time in seconds')
ylabel('Arbitrary Voltage Units')
title('Signal To MicroController(Zoomed in Time Window x 10)')