/**
 * \file BBnateMain
 * 
 * Example file to be uploaded to the Arduino microcontroller for the use
 * with the BBnate by BoneMARK. Serial prints are included merely for
 * testing purposes.
 * 
 * Created by Moseph Jackson-Atogi
 * Last updated April 25, 2016 
 */

// BBnate by BoneMARK

// Import BBnate library and define instance
#include <BBnate.h>

BBnate BBN(12, 11, 3, 2);

/*! \brief

  Using Sainsmart LCD Shield for Arduino
  
  Digital Pin Usage:
  Pin 12 - Transducer
  Pin 11 - LED
  Pin 03 - LED
  Pin 02 - LED

*/

void setup() {
  /*! \brief 1.Turn on power LED */
  BBN.PowerOn();
  
  /*! TESTING: Serial setup */
  Serial.begin(9600);

  /*! \brief 2.Set up the LCD's number of columns and rows */
  BBN.LCDSetup();

  /*! \brief 3.Print Welcome screen */
  BBN.Welcome();
  
  delay(5000);
}

void loop() {

  /*! \brief 4.Define "global" variables (global in the sense of the void loop its in) */
  BBN.delayTime = 500; // in ms, so half a second  

  /*! \brief 5.Display instructions and wait for user input */
  BBN.Instructions();

  char BUTTON;
  while (BUTTON != 'A') {
    BUTTON = BBN.ButtonPress();
  }
    
  /*! \brief 6.Turn on Yellow LED for taking measurement & LCD print measuring */
  BBN.MeasureOn();
  
  /*! \brief 7.Send ping to U/S transducer */

  BBN.SendPing();
  
  /*! \brief 8.Read in analog signal */
  /*! \brief TESTING: Serial print filtered analog values */
  Serial.println("\nAnalog Values:");
  float v[20];
  float *v_p = v;
  v_p = BBN.ReadSignal();
  delay(2 * BBN.delayTime);

  /*! \brief 9.Sum up averaged values */
  /*! \brief TESTING: Serial print the sum of the analog values */
  float v_sum;
  v_sum = BBN.MyIntegrate(v_p);
  Serial.print("\nThe Sum: ");
  Serial.println(v_sum);
  delay(2 * BBN.delayTime);

  /*! \brief 10.Calculate bone mineral density */
  /*! \brief TESTING: Serial print the calculated bone mineral density */
  float BMD;
  BMD = BBN.CalculateBMD(v_sum);
  Serial.print("\nCalculated BMD: ");
  Serial.println(BMD);
  delay(2 * BBN.delayTime);

  /*! \brief 11.Turn off yellow LED once measurement is finished */
  /*! \brief TESTING: Serial print "Done" to verify bmd calculation */
  Serial.println("\nDone!\n");
  BBN.MeasureOff();

  /*! \brief 12.Print BMD to LCD and turn on red LED if error occurs*/
  BBN.PrintBMD(BMD);
  
  delay(10 * BBN.delayTime);

  /*! \brief 13.Print instructions to clear BMD from LCD */
  BBN.PrintClear();

  /*! \brief 14.Wait for the clear button to be pressed to restart loop */
  while (BUTTON != 'R') {
    BUTTON = BBN.ButtonPress();
  }

  /*! \brief 15.Turn off red LED if error occurred*/
  if (BMD == 0.00) {
    BBN.ErrorOff();
    delay(500);
  }
}
