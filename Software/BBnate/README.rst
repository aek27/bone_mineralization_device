The following is all the documentation for software written for the Arduino
library used by the BBnate by BoneMARK. This includes the BBnate class, the
arguments it takes, and its public functions and members.

Created by Moseph Jackson-Atogi

Last updated: April 27, 2016
----

